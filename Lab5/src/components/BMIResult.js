
function BMIResult (props) {

    return (
    <div>
        <h1> คุณ: {props.name} </h1>
        <h1> มี BMI: {props.bmi} </h1>
        <h1> แปลว่า: {props.result} </h1>
    </div>
    
    );
}

export default BMIResult;