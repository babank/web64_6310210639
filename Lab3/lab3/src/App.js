import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Contact from './components/Contact';

function App() {
  return (
    <div className="App">
      <Header /> 
      <Body />
      <Contact />
      <Footer />
    </div>
  );
}

export default App;
