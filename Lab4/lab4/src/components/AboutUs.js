
function AboutUS (props) {
   
    return (
        <div>      
            <h2> จัดทำโดย: { props.name }</h2>
            <h3> ติดต่ออรรถชาได้ ที่ { props.address } </h3>
            <h3> บ้านอรรถชา อยู่ที่ { props.province } </h3>
        </div>
    );
}

export default AboutUS;